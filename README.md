# Introduction

This repository has the code needed to implement a PuWi Host using an ESP8266 WiFi enabled MCU. You can pair a PuWi Host to one or more [PuWi Players](https://gitlab.com/doragasu/puwi_player) to make live quiz games.

# Building

You will need to install version 3.4 of the [ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) to build the sources. After installing and configuring the SDK, plug the target board, clone the sources build them and flash the binary:


```Bash
$ git clone --recursive https://gitlab.com/doragasu/puwi_host.git
$ cd puwi_host
$ make menuconfig
$ make -j$(($(nproc)+1)) flash
```

Optionally, before the build, there are some items you can configure using `make menuconfig`:

* `Component Config / Telegram bot / Telegram bot token`: This is required if you want to use the Telegram bot capabilities. Here you have to paste the bot token obtained by chatting with @botfather in Telegram.
* `Component Config / Telegram bot / Telegram maximum message length`: You might need increasing this to 1024 if you want to connect a lot of players. Otherwise default value of 512 bytes is OK.
* `PUWI Configuration`: Here you can set the SSID and password of the PuWi Host AP, and the maximum number of stations (players) that can connect. Make sure PuWi players share the same SSID and password or they will not be able to connect.
* `Component Config / Log Output / Default log verbosity`: It is recommended to set this to `Error` to reduce latency. This is specially important when using the Telegram bot.

# I/O Wiring

* **GPIO13**: Connect the pushbuton to this pin. Code expects this button to be active low.
* **GPIO12** (optional): Connect the input of an array of 8 WS2812 RGB LEDs.
* **GPIO5** (optional): Connect the pushbutton LED. It is recommended to use a transistor such as a 2N7002 to be able to supply enough current to the LED.
* **A0** (analog pin, optional): Wire to the battery positive pin. Note this requires a resistor divider using e.g. a 330k and a 100k resistor.

Note that other than these pins, you will also have to wire the power and ground pins to the pushbutton and LEDs as needed.

# Usage

## WiFi configuration

Note this is only needed if you want to use the Telegram bot. You can skip this section otherwise, but keep in mind that it is recommended using the Telegram bot, or otherwise you will not be able to configure the system.

WiFi configuration is done using the _ESP SoftAP Provisioning_ app from [Google Play](https://play.google.com/store/apps/details?id=com.espressif.provsoftap) or from the [AppStore](https://apps.apple.com/in/app/esp-softap-provisioning/id1474040630), so install one of them. Then follow this procedure:

1. Power off PuWi Host (if it is not already off).
2. Press and hold the pushbutton. Without releasing it, power on the PuWi Host.
3. The LED bar should start pulsing in red and blue color immediately. You can release the button when this happens.
4. Open the _ESP SoftAP Provisioning_ app, start the provisioning and scan the following QR code when prompted:

![puwi\_config](doc/puwi_config.png)

5. Follow the app instructions. When requested, input the SSID and password of your WiFi router and complete the setup.

Once successfully completed, the WiFi configuration will be saved and the PuWi Host will reboot after a few seconds. If the process fails, power off the unit and repeat the procedure. Make sure the SSID and password of your AP are properly entered.

## Offline mode

Offline mode does not use the Telegram bot. You can start any number of game rounds, and let the players press the buttons to answer. But you will not be able to configure the system or see Telegram event notifications.

Power the board on. If you have connected the WS2812 LEDs, they should start pulsing in white color, indicating that PuWi Host is waiting por PuWi Players to connect. When the first player connects, the LEDs will start the slide animation using a rainbow color pattern. When all the players have connected, you can start the game:

1. Press the button on the PuWi host to start a round. The button LED in the Host will power off, and all the units will start doing the _ping pong_ animation on the LEDs.
2. Make a question. If a player knows the answer, he or she can hit the button. Only the button of the first player pressing it will light and make the _pulse_ animation instead of the _ping pong_ one.
3. To start a new round, repeat since step 1.

## Online mode

This requires that the WiFi configuration has been completed successfully, and the client has been compiled with a correct Telegram bot token configured (see the _Building_ section). To talk to the host through Telegram, with the host powered on, search the Telegram bot and when found, hit the `/start` button (or alternatively, write `/start` and send the message). You will be presented a menu consisting of the following buttons:

* `/start`: write the start message. Most likely you will not need to use this button anymore.
* `Players`: show a list of the players currently connected to the PuWi Host.
* `Configure`: if you press this button, you will be able to configure each player color, the LEDs intensity and the Telegram notifications you want. Each time configuration is changed, it is saved in the host, so most likely you will not be using this option too often.
* `Begin with Telegram`: this begins the game, with Telegram notifications enabled. You will receive only battery level notifications or these and round notifications, depending on the configuration you set using the `Configure` button. By default all notifications are sent.
* `Begin without Telegram`: this begins the game, but will not send any notifications to the Telegram client. This is in fact the same as starting the game as described in the _Offline mode_ subsection.

This is an example of the process to configure the color of a PuWi player:

![color\_config](doc/color_config.png)

This shows how battery notifications are sent:

![batt\_notif](doc/batt_notif.png)

This picture shows the round notifications:

![round\_notif](doc/round_notif.png)

**WARNING**: Once the game is started (and also while PuWi Host is powered off), the Telegram bot will not be able to service requests. Thus you **should not** send requests to the bot once the game is started. Otherwise these will be queued and will not be processed by the PuWi Host until it is rebooted. Note that if too many requests get queued, the bot might not be able to process them, entering a bootloop until the requests are removed from the queue (worst case scenario is a 24h timeout). This problem might be tricky to fix with the low amount of RAM available on an ESP8266 MCU.

# Author

This project has been built and coded by Jesús Alonso (doragasu).

# License

The code in this repository comes with NO WARRANTY and is provided under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
