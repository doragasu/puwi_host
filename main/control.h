#ifndef __CONTROL_H__
#define __CONTROL_H__

#include <stdint.h>
#include <lwip/ip4.h>

#include "telegram.h"
#include "battery.h"

// Event description:
// - sta_assoc: a station has been associated
// - sta_disassoc: a station has been disassociated
// - sta_ip: a station has been assigned an IP address
// - button: button has been pressed
// - end: a player has won the round
// - server_ready: connection to status server ready
// - server_drop: server command parser has been stopped
// - start_with_tg: start game, use Telegram
// - start_without_tg: start game, do not use Telegram
#define CTRL_EVENT_TABLE(X_MACRO) \
	X_MACRO(STA_ASSOC, sta_assoc) \
	X_MACRO(STA_DISASSOC, sta_disassoc) \
	X_MACRO(STA_IP, sta_ip) \
	X_MACRO(BUTTON, button) \
	X_MACRO(END, end) \
	X_MACRO(SERVER_READY, server_ready) \
	X_MACRO(SERVER_DROP, server_drop) \
	X_MACRO(START_WITH_TG, start_with_tg) \
	X_MACRO(START_WITHOUT_TG, start_without_tg) \
	X_MACRO(COLOR_CONFIG, color_config) \
	X_MACRO(INTENSITY_CONFIG, intensity_config) \
	X_MACRO(BATTERY_HOST, battery_host) \
	X_MACRO(BATTERY_PLAYER, battery_player) \

#define X_AS_CTRL_EVENT_TYPE(u_name, l_name) CTRL_EVENT_ ## u_name,
enum ctrl_event_type {
	CTRL_EVENT_TABLE(X_AS_CTRL_EVENT_TYPE)
	__CTRL_EVENT_MAX
};

struct ctrl_data_sta_assoc {
	uint8_t bssid[6];
};

struct ctrl_data_sta_disassoc {
	uint8_t bssid[6];
};

struct ctrl_data_sta_ip {
	uint8_t bssid[6];
	ip4_addr_t ip;
};

struct ctrl_data_end {
	uint8_t bssid[6];
};

struct ctrl_data_server_ready {};

struct ctrl_data_server_drop {};

struct ctrl_data_start_with_tg {};

struct ctrl_data_start_without_tg {};

struct ctrl_data_color_config {
	uint8_t bssid[6];
	enum cfg_color color;
};

struct ctrl_data_intensity_config {
	enum cfg_intensity intensity;
};

struct ctrl_data_battery_host {
	enum bat_event event;
	enum bat_status status;
	uint32_t voltage_millis;
};

struct ctrl_data_battery_player {
	uint8_t bssid[6];
	enum bat_event event;
	enum bat_status status;
};

// Button carries no data in the struct. The GPIO number triggered
// is passed in the pointer itself
struct ctrl_data_button {
};

#define X_AS_CTRL_DATA_UNION(u_name, l_name) \
	struct ctrl_data_ ## l_name l_name;
union ctrl_data {
	CTRL_EVENT_TABLE(X_AS_CTRL_DATA_UNION)
};

struct ctrl_event {
	enum ctrl_event_type event;
	union ctrl_data *data;

};

int ctrl_start(void);

int ctrl_event(struct ctrl_event *event);
void ctrl_event_from_isr(struct ctrl_event *event);

#endif /*__CONTROL_H__*/

