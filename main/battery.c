#include <string.h>
#include <FreeRTOS.h>
#include <freertos/task.h>

#include <driver/adc.h>

#include "util.h"
#include "battery.h"

// TODO: Remove logs and lower this as much as possible
#define BAT_TSK_STACK 2048
#define BAT_TSK_PRIO (tskIDLE_PRIORITY + 1)
#define BAT_PERIOD_MS 5000
#define BAT_PERIOD_TICKS (BAT_PERIOD_MS / portTICK_PERIOD_MS)
#define BAT_SAMPLES_PER_VAL_DEFAULT (60000 / BAT_PERIOD_MS)
#define BAT_ADC_MAX 1023
// Calibration value measured
#define BAT_ADC_CAL 1052

// Resistor divider values
#define DIV_TOP 33
#define DIV_BOT 10

#define ADC_TO_MV(accum, samples) ((accum) * (DIV_TOP + DIV_BOT) * 1000 / \
		(samples * DIV_BOT * BAT_ADC_CAL))

enum bat_segment {
	BAT_SEG_LOW,
	BAT_SEG_MID,
	BAT_SEG_HIGH,
	BAT_SEG_MAX
};

static struct {
	struct bat_cfg cfg;
	uint32_t period_samples;
	uint32_t samples;
	uint32_t accum;
	uint32_t last_mv;
	uint32_t old_mv;
	enum bat_segment cur_seg;
	enum bat_status status;
	bool end_task;
} bat;

static bool sample_accum(void)
{
	uint16_t last = 0;
	bool update = false;

	adc_read(&last);
	LOGD("adc_read: %d", last);
	bat.accum += last;
	bat.samples++;

	if (bat.samples >= bat.period_samples) {
		bat.old_mv = bat.last_mv;
		bat.last_mv = ADC_TO_MV(bat.accum, bat.samples);
		LOGD("average mv: %d", bat.last_mv);
		update = true;
		bat.samples = 0;
		bat.accum = 0;
	}

	return update;
}

static void threshold_raise(enum bat_event event, enum bat_status status)
{
	LOGW("event %d, status %d, mv %d", event, status, bat.last_mv);
	bat.cfg.cb(event, status, bat.last_mv);
}

static void threshold_charge_eval(void)
{
	// Use int instead of enum to ensure it can have negative values
	int event;
	uint32_t thr;

	for (event = BAT_EV_HIGH; event >= BAT_EV_LOW; event--) {
		thr = bat.cfg.threshold[event] + bat.cfg.hysteresis;
		if (bat.last_mv >= thr && bat.old_mv < thr &&
				(event + 1 ) != bat.cur_seg) {
			bat.cur_seg = event + 1;
			threshold_raise(event, BAT_CHARGING);
			break;
		}
	}
}

static void threshold_discharge_eval(void)
{
	enum bat_event event;
	uint32_t thr;

	for (event = BAT_EV_LOW; event <= BAT_EV_HIGH; event++) {
		thr = bat.cfg.threshold[event] - bat.cfg.hysteresis;
		if (bat.last_mv <= thr && bat.old_mv > thr &&
				event != (int)bat.cur_seg) {
			bat.cur_seg = (int)event;
			threshold_raise(event, BAT_DISCHARGING);
			break;
		}
	}
}

static void update_proc(void)
{
	if (bat.last_mv > bat.old_mv) {
		bat.status = BAT_CHARGING;
	} else if (bat.last_mv < bat.old_mv) {
		bat.status = BAT_DISCHARGING;
	}

	if (bat.cfg.period_ms) {
		bat.cfg.cb(BAT_EV_PERIODIC, bat.status, bat.last_mv);
	}

	switch (bat.status) {
		case BAT_CHARGING:
			threshold_charge_eval();
			break;

		case BAT_DISCHARGING:
			threshold_discharge_eval();
			break;

		default:
			break;
	}
}

static void bat_tsk(void *arg)
{
	UNUSED_PARAM(arg);
	bool update;

	vTaskDelayMs(BAT_PERIOD_MS);

	while (!bat.end_task) {
		update = sample_accum();
		if (update && bat.cfg.cb) {
			update_proc();
		}
		vTaskDelayMs(BAT_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

bool bat_start(const struct bat_cfg *cfg)
{
	int err;
	adc_config_t adc_cfg = {
		.mode = ADC_READ_TOUT_MODE,
		.clk_div = 32	// Slowest value (2.5 MHz)
	};

	if (cfg->period_ms && cfg->period_ms < BAT_PERIOD_MS) {
		return 1;
	}
	adc_init(&adc_cfg);

	memset(&bat, 0, sizeof(bat));
	bat.cfg = *cfg;
	if (bat.cfg.period_ms) {
		bat.period_samples = bat.cfg.period_ms / BAT_PERIOD_MS;
	} else {
		bat.period_samples = BAT_SAMPLES_PER_VAL_DEFAULT;
	}
	bat.cur_seg = BAT_SEG_HIGH;
	bat.last_mv = ADC_TO_MV(BAT_ADC_MAX, 1);
	bat.status = BAT_DISCHARGING;

	err = pdPASS != xTaskCreate(bat_tsk, "bat_tsk", BAT_TSK_STACK, NULL,
			BAT_TSK_PRIO, NULL);

	return err;
}

void bat_stop(void)
{
	bat.end_task = true;
}
