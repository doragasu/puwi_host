#include <string.h>
#include <FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <driver/gpio.h>

#include <yamcha.h>

#include "player.h"
#include "control.h"
#include "util.h"
#include "config.h"

#define LED_PIN 5
#define LED_PIN_MASK (1UL<<LED_PIN)

#define LED_ON 1
#define LED_OFF 0

#define CTRL_TASK_PRIO	(tskIDLE_PRIORITY + 2)
#define CTRL_TASK_STACK	4096

#define PX_WHITE {255, 255, 255}

#define SCALE_U8(value, scale) (((uint_fast16_t)(value) * \
			(uint_fast16_t)(scale))>>8)

#define GAME_STAT_TABLE(X_MACRO) \
	X_MACRO(IDLE, idle) \
	X_MACRO(START, start) \
	X_MACRO(END, end) \

#define X_AS_GAME_STAT_ENUM(u_name, l_name) G_STAT_ ## u_name,
enum game_stat {
	GAME_STAT_TABLE(X_AS_GAME_STAT_ENUM)
	__G_STAT_MAX
};

#define X_AS_STAT_NAME(u_name, l_name) #u_name,
static const char * const game_stat_name[__G_STAT_MAX] = {
	GAME_STAT_TABLE(X_AS_STAT_NAME)
};

#define X_AS_EVENT_NAME(u_name, l_name) #u_name,
static const char *event_name[__CTRL_EVENT_MAX] = {
	CTRL_EVENT_TABLE(X_AS_EVENT_NAME)
};

#define X_AS_GAME_STAT_PARSER_PROTO(u_name, l_name) \
	static void parse_ ## l_name(struct ctrl_event*);
GAME_STAT_TABLE(X_AS_GAME_STAT_PARSER_PROTO);

#define X_AS_GAME_STAT_JUMP_TABLE(u_name, l_name) parse_ ## l_name,
void (*parse[__G_STAT_MAX])(struct ctrl_event*) = {
	GAME_STAT_TABLE(X_AS_GAME_STAT_JUMP_TABLE)
};

// Values on indexes 1 to 5 are computed as:
// intensity[i] = 255 ^ (i/5)
static const uint_fast8_t intensity[__CFG_INTENSITY_MAX] = {
	0, 3, 9, 28, 84, 255
};

static const struct yam_pixel slide_pixels[YAM_PIXEL_NUM] = {
			{0x00, 0xFF, 0x00}, {0x7F, 0xFF, 0x00},
			{0xFF, 0x7F, 0x00}, {0xFF, 0x00, 0x00},
			{0xFF, 0x00, 0xFF}, {0x7F, 0x00, 0xFF},
			{0x00, 0x3F, 0xFF}, {0x00, 0xFF, 0xFF}
};

static struct {
	enum game_stat stat;
	QueueHandle_t q;
	bool tg_enabled;
	struct yam_cmd_req *yam;
	enum cfg_intensity bar_intensity;
} game = {};

#define stat_change(st) do { \
	LOGD("GAME: %s ==> %s", game_stat_name[game.stat], game_stat_name[st]); \
	game.stat = (st); \
} while (0)

static void intensity_apply(struct yam_pixel *pixel, uint_fast8_t intensity)
{
	pixel->r = SCALE_U8(pixel->r, intensity);
	pixel->g = SCALE_U8(pixel->g, intensity);
	pixel->b = SCALE_U8(pixel->b, intensity);
}

static void yam_mode_change(struct yam_cmd_req *req)
{
	yam_command(req);
	free(game.yam);
	game.yam = req;
}

static void yam_end_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));
	const uint_fast8_t intens = intensity[game.bar_intensity];

	req->mode = YAM_CMD_SLIDE;
	req->slide.delay_ms = 30;
	memcpy(req->slide.pixels, slide_pixels, sizeof(slide_pixels));

	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		intensity_apply(&req->slide.pixels[i], intens);
	}

	yam_mode_change(req);
}

static void yam_breath_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));
	struct yam_pixel color = PX_WHITE;

	req->mode = YAM_CMD_BREATH;
	req->breath.delay_ms = 32;

	intensity_apply(&color, intensity[game.bar_intensity]);
	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		req->breath.pixels[i] = color;
	}

	yam_mode_change(req);
}

static void yam_ready_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));
	struct yam_pixel color = PX_WHITE;
	intensity_apply(&color, intensity[game.bar_intensity]);

	req->mode = YAM_CMD_PINGPONG;
	req->pingpong.initial_value = color;
	req->pingpong.steps = 10;
	req->pingpong.delay_ms = 32;

	yam_mode_change(req);
}

static void yam_request_refresh(void)
{
	// Re-apply current effect
	switch (game.yam->mode) {
	case YAM_CMD_BREATH:
		yam_breath_cmd();
		break;

	case YAM_CMD_PINGPONG:
		yam_ready_cmd();
		break;

	case YAM_CMD_SLIDE:
		yam_end_cmd();
		break;

	default:
		// Ignore unused modes
		break;
	}
}

static void color_config_save(const struct player_event *player)
{
	const struct cfg *cfg = cfg_get();
	size_t config_len = cfg_length(cfg);
	const struct cfg_entry *player_cfg = cfg_search(cfg, player->bssid);
	struct cfg *new_cfg;
	struct cfg_entry *entry;

	if (player_cfg) {
		// Update existing entry
		new_cfg = memdup(cfg, config_len);
		entry = (struct cfg_entry*)cfg_search(new_cfg, player->bssid);
	} else {
		// Create new entry
		new_cfg = malloc(config_len + sizeof(struct cfg_entry));
		memcpy(new_cfg, cfg, config_len);
		new_cfg->num_entries++;
		entry = (struct cfg_entry*)(((char*)new_cfg) + config_len);
		memset(entry, 0, sizeof(struct cfg_entry));
		memcpy(entry->bssid, player->bssid, 6);
	}
	entry->color = (enum cfg_color)player->color;

	cfg_save(new_cfg);
	free(new_cfg);
}

static void intensity_config_save(enum cfg_intensity intensity)
{
	const struct cfg *cfg = cfg_get();

	if (game.bar_intensity != intensity) {
		game.bar_intensity = intensity;
		yam_request_refresh();
	}

	if (cfg->bar_intensity == intensity) {
		return;
	}
	size_t config_len = cfg_length(cfg);
	struct cfg *new_cfg = memdup(cfg, config_len);
	new_cfg->bar_intensity = intensity;
	cfg_save(new_cfg);

	free(new_cfg);
}

static void bat_level_notify(enum tg_color color_idx, enum bat_event event,
		enum bat_status status)
{
	char msg[32];
	const char * const level_str[] = {"LOW", "MEDIUM", "HIGH"};
	const char * const status_str[] = {"🔌", "🔋"};

	if (game.tg_enabled && (cfg_get()->tg_event & CFG_TG_EVENT_BATT)) {
		snprintf(msg, 32, "%s: %s %s", color_str[color_idx],
				status_str[status],
				level_str[event]);
		tg_notify(msg, BATE_TEXT_PLAIN);
	}
	if (BAT_EV_LOW == event && BAT_DISCHARGING == status &&
			game.bar_intensity != CFG_INTENSITY_20) {
		// Set intensity to 20% to signal low battery level
		game.bar_intensity = CFG_INTENSITY_20;
		yam_request_refresh();
	} else {
		// Restore configured intensity when charged enough
		enum cfg_intensity bar_intensity = cfg_get()->bar_intensity;
		if (bar_intensity != game.bar_intensity) {
			game.bar_intensity = bar_intensity;
			yam_request_refresh();
		}
	}
}

static void bat_player_notify(const struct ctrl_data_battery_player *bat)
{
	const struct cfg_entry *player = cfg_search(cfg_get(), bat->bssid);
	enum cfg_color color = __CFG_COLOR_MAX;

	if (player && player->color < __CFG_COLOR_MAX) {
		color = player->color;
	}

	bat_level_notify((enum tg_color)color, bat->event, bat->status);
}

static bool parse_global(struct ctrl_event *ev)
{
	bool processed = true;
	struct player_event player = {};

	switch (ev->event) {
	case CTRL_EVENT_STA_ASSOC:
		// Nothing to do
		break;

	case CTRL_EVENT_STA_DISASSOC:
		player.event = PLAYER_EVENT_DEL;
		memcpy(player.bssid, ev->data->sta_disassoc.bssid, 6);
		player_parse(&player);
		break;

	case CTRL_EVENT_STA_IP:
		player.event = PLAYER_EVENT_ADD;
		memcpy(player.bssid, ev->data->sta_ip.bssid, 6);
		memcpy(&player.ip, &ev->data->sta_ip.ip, sizeof(ip4_addr_t));
		player_parse(&player);
		yam_end_cmd();
		gpio_set_level(LED_PIN, LED_ON);
		break;

	case CTRL_EVENT_COLOR_CONFIG:
		player.event = PLAYER_EVENT_COLOR;
		memcpy(player.bssid, ev->data->color_config.bssid, 6);
		player.color = (enum player_color)ev->data->color_config.color;
		color_config_save(&player);
		player_parse(&player);
		break;

	case CTRL_EVENT_INTENSITY_CONFIG:
		player.event = PLAYER_EVENT_INTENSITY;
		memcpy(player.bssid, PLAYER_BSSID_ALL, 6);
		player.intensity = (enum player_intensity)
			ev->data->intensity_config.intensity;
		intensity_config_save(ev->data->intensity_config.intensity);
		player_parse(&player);
		break;

	case CTRL_EVENT_BATTERY_HOST:
		bat_level_notify(TG_COLOR_WHITE, ev->data->battery_host.event,
				ev->data->battery_host.status);
		break;

	case CTRL_EVENT_BATTERY_PLAYER:
		bat_player_notify(&ev->data->battery_player);
		break;

	default:
		processed = false;
	}

	return processed;
}

static void parse_idle(struct ctrl_event *ctrl)
{
	struct player_event player;

	switch (ctrl->event) {
	case CTRL_EVENT_START_WITH_TG:
		game.tg_enabled = true;
		// fallthrough
	case CTRL_EVENT_START_WITHOUT_TG:
		// fallthrough
	case CTRL_EVENT_BUTTON:
		if (player_num_get()) {
			if (!game.tg_enabled) {
				tg_parser_stop();
			}
			player.event = PLAYER_EVENT_GAME_START;
			memcpy(player.bssid, PLAYER_BSSID_ALL, 6);
			player_parse(&player);
			stat_change(G_STAT_START);
			yam_ready_cmd();
			gpio_set_level(LED_PIN, LED_OFF);
			if (game.tg_enabled && (cfg_get()->tg_event &
					CFG_TG_EVENT_ROUND)) {
				tg_notify("Round start!", BATE_TEXT_MARKDOWN);
			}
		} else {
			LOGI("no players registered");
		}
		break;

	default:
		break;
	}
}

static void parse_start(struct ctrl_event *ctrl)
{
	struct player_event player;
	const uint8_t *bssid;
	char msg[64];

	switch (ctrl->event) {
	case CTRL_EVENT_END:
		player.event = PLAYER_EVENT_BLOCK;
		memcpy(player.bssid, PLAYER_BSSID_ALL, 6);
		player_parse(&player);
		stat_change(G_STAT_END);
		yam_end_cmd();
		gpio_set_level(LED_PIN, LED_ON);
		if (game.tg_enabled && (cfg_get()->tg_event &
					CFG_TG_EVENT_ROUND)) {
			bssid = ctrl->data->end.bssid;
			const struct cfg_entry *cfg =
				cfg_search(cfg_get(), bssid);
			snprintf(msg, 64, "%s `" MACSTR "` answers!",
					color_str[cfg->color], MAC2STR(bssid));
			tg_notify(msg, BATE_TEXT_MARKDOWN);
		}
		break;

	default:
		break;
	}
}

static void parse_end(struct ctrl_event *ctrl)
{
	parse_idle(ctrl);
}

static void process(struct ctrl_event *ev)
{
	if (ev->event < 0 || ev->event >= __CTRL_EVENT_MAX) {
		LOGE("ignoring unknown event %d", ev->event);
		goto out;
	}
	LOGD("%s", event_name[ev->event]);
	bool processed = parse_global(ev);
	if (processed) {
		goto out;
	}

	parse[game.stat](ev);

out:
	free(ev->data);
}

static void ctrl_tsk(void *arg)
{
	UNUSED_PARAM(arg);
	struct ctrl_event event;

	while(true) {
		if (pdTRUE == xQueueReceive(game.q, &event,
					pdMS_TO_TICKS(1000))) {
			process(&event);
		}
	}
}

static void led_cfg(void)
{
	static const gpio_config_t cfg = {
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask = LED_PIN_MASK
	};

	gpio_config(&cfg);
}

int ctrl_start(void)
{
	int err = 1;
	const struct cfg *cfg = cfg_get();

	led_cfg();
	gpio_set_level(LED_PIN, LED_OFF);

	game.bar_intensity = cfg->bar_intensity;
	yam_breath_cmd();
	game.q = xQueueCreate(10, sizeof(union ctrl_data));
	if (0 == game.q) {
		LOGE("queue creation failed");
		goto error;
	}

	if (pdPASS != xTaskCreate(ctrl_tsk, "ctrl_tsk", CTRL_TASK_STACK,
				NULL, CTRL_TASK_PRIO, NULL)) {
		LOGE("task creation failed");
		goto error;
	}

	return 0;

error:
	if (game.q) {
		vQueueDelete(game.q);
		game.q = 0;
	}

	return err;
}

int ctrl_event(struct ctrl_event *event)
{
	if (pdTRUE != xQueueSend(game.q, event, pdMS_TO_TICKS(5000))) {
		free(event->data);
		LOGE("failed to send event %d", event->event);
		return -1;
	}

	return 0;
}

void ctrl_event_from_isr(struct ctrl_event *event)
{
	BaseType_t context_switch = pdFALSE;

	xQueueSendFromISR(game.q, event, &context_switch);
	if (context_switch) {
		taskYIELD();
	}
}

