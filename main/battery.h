/*
 * Battery readings module.
 *
 * NOTE: The analog input pin in ESP8266 allows to read up to 1.023V. Nodemcu
 * and most clones have a built in resistor divider consisting of a 200k and
 * 100k resistors, to allow reading up to 3.3V. This is not enough to read
 * LiPo batteries, and thus this module assumes you have modified your board,
 * replacing the 220k resistor with a 300k one. This allows reading up to
 * 4.1V: 1.023 * (300 + 100) / 100 = 4.092
 */

#ifndef _BATTERY_H_
#define _BATTERY_H_

#include <stdint.h>
#include <stdbool.h>

/// Default thresholds, can be used to init bat_cfg.threshold array
#define BAT_THRESHOLD_DEFAULT {3400, 3650, 3950}

enum bat_event {
	BAT_EV_LOW,
	BAT_EV_MID,
	BAT_EV_HIGH,
	BAT_EV_PERIODIC,
	__BAT_EV_MAX
};

enum bat_status {
	BAT_CHARGING,
	BAT_DISCHARGING,
	__BAT_CHARGE_MAX
};

typedef void (*bat_cb)(enum bat_event event, enum bat_status status,
		uint32_t voltage_millis);

struct bat_cfg {
	bat_cb cb;
	/// Thresholds in mV for battery event notifications.
	/// Set a threshold to 0 to disable.
	uint32_t threshold[3];
	/// Hysteresis in mV.
	uint32_t hysteresis;
	/// Period for periodic events. Set to 0 to
	/// disable periodic events.
	uint32_t period_ms;
};

bool bat_start(const struct bat_cfg *cfg);
void bat_stop(void);
uint32_t bat_get_mv(void);

#endif /*_BATTERY_H_*/
