#include <string.h>
#include <esp_event_loop.h>
#include <esp_wifi.h>

#include "wifi_apsta.h"
#include "util.h"

static wifi_event_cb event_cb = NULL;
static uint_fast8_t conn_attempts;

static void sta_ip_notify(ip4_addr_t ip)
{
	wifi_sta_list_t wifi_stas;
	tcpip_adapter_sta_list_t ip_stas;
	esp_err_t err;
	const char *ip_str = ip4addr_ntoa(&ip);

	if ((err = esp_wifi_ap_get_sta_list(&wifi_stas)) ||
			(err = tcpip_adapter_get_sta_list(&wifi_stas,
							  &ip_stas))) {
		LOGE("processing ip %s: %s", ip_str, esp_err_to_name(err));
		return;
	}

	for (int i = 0; i < ip_stas.num; i++) {
		if (ip_stas.sta[i].ip.addr == ip.addr) {
			if (event_cb) {
				union wifi_event_data data = {};
				memcpy(data.sta_ip.bssid, ip_stas.sta[i].mac, 6);
				data.sta_ip.ip = ip;
				event_cb(WIFI_EVENT_STA_IP, &data);
			}
			return;
		}
	}

	LOGE("station with IP %s not found", ip_str);
}

static void deleted_sta_notify(uint8_t mac[6])
{
	union wifi_event_data data = {};

	LOGI("station: " MACSTR " leave", MAC2STR(mac));

	if (event_cb) {
		memcpy(data.sta_del.bssid, mac, 6);
		event_cb(WIFI_EVENT_STA_DEL, &data);
	}
}

static void new_sta_notify(uint8_t bssid[6])
{
	union wifi_event_data data = {};

	LOGI("station: " MACSTR " join", MAC2STR(bssid));

	if (event_cb) {
		memcpy(data.sta_add.bssid, bssid, 6);
		event_cb(WIFI_EVENT_STA_ADD, &data);
	}
}

static void disconnected_proc(system_event_sta_disconnected_t *dis)
{
	conn_attempts++;
	LOGW("attempt %d, reason: %d", conn_attempts, dis->reason);
	if (conn_attempts >= WIFI_APSTA_ASSOC_ATTEMPTS) {
		LOGE("desisting on connection to AP");
		esp_wifi_disconnect();
		if (event_cb) {
			event_cb(WIFI_EVENT_AP_DESIST, NULL);
		}
		return;
	}

	if (dis->reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
		// switch to 802.11 bgn mode
		esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCOL_11B |
				WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N);
	}
	esp_wifi_connect();
	if (event_cb) {
		event_cb(WIFI_EVENT_AP_DISASSOC, NULL);
	}
}

static void got_ip_proc(system_event_sta_got_ip_t *got_ip)
{
	LOGI("station got IP %s", ip4addr_ntoa(&got_ip->ip_info.ip));

	if (event_cb) {
		event_cb(WIFI_EVENT_AP_GOT_IP, NULL);
	}
}

static void connected_proc(system_event_sta_connected_t *con)
{
	UNUSED_PARAM(con);
	LOGI("station connected");

	if (event_cb) {
		event_cb(WIFI_EVENT_AP_ASSOC, NULL);
	}
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
	UNUSED_PARAM(ctx);

	switch(event->event_id) {
	case SYSTEM_EVENT_AP_START:
		LOGI("AP is starting");
		break;

	case SYSTEM_EVENT_STA_START:
		LOGI("STA is starting");
		esp_wifi_connect();
		break;

	case SYSTEM_EVENT_STA_CONNECTED:
		connected_proc(&event->event_info.connected);
		break;

	case SYSTEM_EVENT_STA_GOT_IP:
		got_ip_proc(&event->event_info.got_ip);
		break;

	case SYSTEM_EVENT_STA_DISCONNECTED:
		disconnected_proc(&event->event_info.disconnected);
		break;

	case SYSTEM_EVENT_AP_STOP:
		LOGE("AP has stopped");
		break;

	case SYSTEM_EVENT_AP_STACONNECTED:
		new_sta_notify(event->event_info.sta_connected.mac);
		break;

	case SYSTEM_EVENT_AP_STADISCONNECTED:
		deleted_sta_notify(event->event_info.sta_disconnected.mac);
		break;

	case SYSTEM_EVENT_AP_STAIPASSIGNED:
		sta_ip_notify(event->event_info.ap_staipassigned.ip);
		break;

	default:
		break;
	}

	return ESP_OK;
}

void wifi_init_softap(wifi_event_cb _event_cb)
{
	conn_attempts = 0;
	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	wifi_config_t wifi_config = {
		.ap = {
			.ssid = CONFIG_PUWI_AP_SSID,
			.ssid_len = strlen(CONFIG_PUWI_AP_SSID),
			.password = CONFIG_PUWI_AP_PASSWORD,
			.max_connection = CONFIG_MAX_STA_CONN,
			.authmode = WIFI_AUTH_WPA_WPA2_PSK
		},
	};
	if (strlen(CONFIG_PUWI_AP_PASSWORD) == 0) {
		wifi_config.ap.authmode = WIFI_AUTH_OPEN;
	}

	event_cb = _event_cb;

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	LOGI("finished. PUWI SSID: " CONFIG_PUWI_AP_SSID ","
			" AP SSID: " CONFIG_PUWI_AP_PASSWORD);
}

