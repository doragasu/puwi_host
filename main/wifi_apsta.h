#ifndef _WIFI_AP_H_
#define _WIFI_AP_H_

#include <stdint.h>
#include <lwip/ip4.h>

// Number of association attempts before desisting
#define WIFI_APSTA_ASSOC_ATTEMPTS 10

struct wifi_event_sta_add {
	uint8_t bssid[6];
};

struct wifi_event_sta_del {
	uint8_t bssid[6];
};

struct wifi_event_sta_ip {
	uint8_t bssid[6];
	ip4_addr_t ip;
};

union wifi_event_data {
	struct wifi_event_sta_add sta_add;
	struct wifi_event_sta_del sta_del;
	struct wifi_event_sta_ip sta_ip;
};

enum wifi_event {
	WIFI_EVENT_STA_ADD,	// A station has been associated
	WIFI_EVENT_STA_DEL,	// A station has disassociated
	WIFI_EVENT_STA_IP,	// A station has been assigned IP
	WIFI_EVENT_AP_ASSOC,	// Device has associated to AP
	WIFI_EVENT_AP_DISASSOC,	// Device has disassociated from AP
	WIFI_EVENT_AP_GOT_IP,	// Device has obtained IP
	WIFI_EVENT_AP_DESIST,	// Desisting from AP connection
	__WIFI_EVENT_MAX
};

typedef void (*wifi_event_cb)(enum wifi_event event,
		union wifi_event_data *data);

void wifi_init_softap(wifi_event_cb event_cb);

#endif /*_WIFI_AP_H_*/

