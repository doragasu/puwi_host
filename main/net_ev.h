#ifndef __NET_EV_H__
#define __NET_EV_H__

#include <lwip/udp.h>
#include "player_cmd.h"

typedef void (*nev_cb)(const ip4_addr_t *ip, uint_fast16_t port,
		void *data, uint_fast16_t data_len);

int nev_init(struct udp_pcb *socket, nev_cb event_callback);

void nev_deinit(void);

#endif /*__NET_EV_H__*/

