#include <string.h>
#include <FreeRTOS.h>
#include <freertos/timers.h>
#include <freertos/semphr.h>
#include <lwip/udp.h>

#include "linux_list.h"
#include "net_ev.h"
#include "util.h"
#include "player.h"
#include "config.h"

// TODO: should we wait until all players have confirmed before starting
// processing triggers? Otherwise a player might launch a trigger before
// other has had the option to do it.

#define HOST_PORT 777
#define RETRY_TOUT_TICKS pdMS_TO_TICKS(100)

#define PLAYER_STAT_TABLE(X_MACRO) \
	X_MACRO(IDLE, idle) \
	X_MACRO(READY_CHECK, ready_check) \
	X_MACRO(READY, ready) \
	X_MACRO(BLOCKED_CHECK, blocked_check) \
	X_MACRO(BLOCKED, blocked) \
	X_MACRO(TRIGGED, trigged) \

#define X_AS_PLAYER_STAT_ENUM(u_name, l_name) P_STAT_ ## u_name,
enum player_stat {
	PLAYER_STAT_TABLE(X_AS_PLAYER_STAT_ENUM)
	__P_STAT_MAX
};

struct player_node {
	struct list_head _head;
	enum player_stat stat;
	xTimerHandle t;
	struct player_cmd_req req;
	struct player player;
};

#define X_AS_NAME(u_name, l_name) #u_name,
static const char * const player_stat_name[__P_STAT_MAX] = {
	PLAYER_STAT_TABLE(X_AS_NAME)
};

static const char * const player_event_name[__PLAYER_EVENT_MAX] = {
	PLAYER_EVENT_TABLE(X_AS_NAME)
};

#define X_AS_PARSER_PROTO(u_name, l_name) \
	static bool player_event_ ## l_name ## _parse(struct player_node*, \
			struct player_event*);
PLAYER_EVENT_TABLE(X_AS_PARSER_PROTO);

#define X_AS_PARSER_JUMP_TABLE(u_name, l_name) player_event_ ## l_name ## _parse,
static bool (*const parse[__PLAYER_EVENT_MAX])(struct player_node*,
		struct player_event*) = {
	PLAYER_EVENT_TABLE(X_AS_PARSER_JUMP_TABLE)
};

static struct udp_pcb *udp = NULL;
static struct list_head player_list = LIST_HEAD_INIT(player_list);
static uint_fast8_t num_players = 0;
static player_game_event_cb event_cb = NULL;
// This has to be set using a global variable because otherwise enqueuing the
// block command in the event queue can cause a trigger from other player to
// be processed before the block command.
static bool global_trigger = false;
static SemaphoreHandle_t sem = NULL;

#define stat_change(node, st) do { \
	LOGD("PLAYER: " MACSTR " (%d): %s ==> %s", MAC2STR(node->player.bssid), \
			node->player.number, player_stat_name[node->stat], \
			player_stat_name[st]); \
	node->stat = (st); \
} while (0)


static struct player_node *player_search_bssid_internal(const uint8_t bssid[6])
{
	struct player_node *node;

	xSemaphoreTake(sem, portMAX_DELAY);
	list_for_each_entry(node, &player_list, _head) {
		if (0 == memcmp(bssid, node->player.bssid, 6)) {
			goto out;
		}
	}

	// If we reach here, bssid was not found
	node = NULL;

out:
	xSemaphoreGive(sem);

	return node;
}

struct player *player_search_bssid(const uint8_t bssid[6])
{
	struct player_node *node;

	node = player_search_bssid_internal(bssid);

	if (node) {
		return &node->player;
	} else {
		return NULL;
	}
}

static bool player_in_use(uint_fast8_t number)
{
	struct player_node *node;
	bool in_use = false;

	xSemaphoreTake(sem, portMAX_DELAY);
	list_for_each_entry(node, &player_list, _head) {
		if (number == node->player.number) {
			in_use = true;
			goto out;
		}
	}

out:
	xSemaphoreGive(sem);

	return in_use;
}

static uint_fast8_t player_number_new(void)
{
	uint8_t number = 1;

	while (player_in_use(number)) {
		number++;
	}

	return number;
}

static void player_cmd_retry(struct player_node *node)
{
	// Dunny why, we must copy the buffer or sometimes udp_send() crashes
	// With this in mind, maybe we could save only the payload instead of
	// the full buffer for retries
	ip_addr_t addr = {
		.type = IPADDR_TYPE_V4,
		.u_addr.ip4.addr = node->player.ip.addr
	};
	struct pbuf *buf = pbuf_alloc(PBUF_TRANSPORT,
			sizeof(struct player_cmd_req), PBUF_RAM);
	memcpy(buf->payload, &node->req, sizeof(struct player_cmd_req));
	err_t err = udp_sendto(udp, buf, &addr, HOST_PORT);
	if (err) {
		LOGE("send to %s:" STR(HOST_PORT) " returned error %d",
				ip_ntoa(&addr), err);
	}
	pbuf_free(buf);
	LOGW(MACSTR ": retry command %d", MAC2STR(node->player.bssid),
			node->req.cmd);
}

// The timer could send a timer event to the command queue, but since it is
// only used for retry login, we are taking a shortcut and sending the
// retry directly
static void IRAM_ATTR retry_tmr(xTimerHandle t)
{
	struct player_node *node = (struct player_node*)pvTimerGetTimerID(t);

	// Check if we have to retry any of the events
	switch (node->stat) {
	case P_STAT_READY_CHECK:
		// flowdown
	case P_STAT_BLOCKED_CHECK:
		player_cmd_retry(node);
		break;

	default:
		LOGE(MACSTR ": ignoring timer", MAC2STR(node->player.bssid));
		break;
	}
}

static void player_cmd_send(struct player_node *node, enum player_cmd cmd,
		bool confirm)
{
	ip_addr_t addr = {
		.type = IPADDR_TYPE_V4,
		.u_addr.ip4.addr = node->player.ip.addr
	};
	struct pbuf *buf = pbuf_alloc(PBUF_TRANSPORT,
			sizeof(struct player_cmd_req), PBUF_RAM);
	const struct cfg *cfg = cfg_get();
	node->req.cmd = cmd;
	node->req.player_num = node->player.number;
	// Color data is sent in color change command, and also in
	// start command
	if (PLAYER_CMD_COLOR == cmd || PLAYER_CMD_START == cmd) {
		node->req.led.color = node->player.color;
		node->req.led.intensity = (enum player_intensity)cfg->bar_intensity;
	}
	memcpy(buf->payload, &node->req, sizeof(struct player_cmd_req));

	err_t err = udp_sendto(udp, buf, &addr, HOST_PORT);
	pbuf_free(buf);
	if (err) {
		LOGE("send to %s:" STR(HOST_PORT) " returned error %d",
				ip_ntoa(&addr), err);
	}
	// If command requires confirmation, start retry timer
	if (confirm) {
		xTimerStart(node->t, pdMS_TO_TICKS(5000));
	}
	LOGD(MACSTR ": send cmd %d", MAC2STR(node->player.bssid), cmd);
}

static void player_add(const uint8_t bssid[6], const ip4_addr_t *ip)
{
	const struct cfg *cfg = cfg_get();
	const struct cfg_entry *player_cfg = cfg_search(cfg, bssid);
	struct player *player = player_search_bssid(bssid);
	struct player_node *node;
	if (player) {
		LOGI(MACSTR " already connected", MAC2STR(bssid));
		player->ip = *ip;
		node = container_of(player, struct player_node, player);
		player_cmd_send(node, PLAYER_CMD_COLOR, false);
		return;
	}

	node = calloc(1, sizeof(struct player_node));
	memcpy(node->player.bssid, bssid, 6);
	node->player.ip = *ip;

	if (player_cfg) {
		node->player.color = (enum player_color)player_cfg->color;
	} else {
		node->player.color =  __PLAYER_COLOR_MAX;
	}

	node->player.number = player_number_new();
	node->t = xTimerCreate(NULL, RETRY_TOUT_TICKS, pdTRUE,
			node, retry_tmr);

	xSemaphoreTake(sem, portMAX_DELAY);
	num_players++;
	list_add_tail(&node->_head, &player_list);
	xSemaphoreGive(sem);

	player_cmd_send(node, PLAYER_CMD_COLOR, false);
	LOGI("added player %d with id " MACSTR, node->player.number,
			MAC2STR(bssid));
}

static void player_del(struct player_node *node)
{
	LOGI("deleting player " MACSTR, MAC2STR(node->player.bssid));
	xTimerDelete(node->t, pdMS_TO_TICKS(5000));
	xSemaphoreTake(sem, portMAX_DELAY);
	num_players--;
	list_del(&node->_head);
	xSemaphoreGive(sem);
	free(node);
}

static struct player_node *network_check(const ip4_addr_t *ip,
		struct player_event *net, uint_fast16_t data_len)
{
	if (data_len > sizeof(struct player_event)) {
		LOGE("unexpected packet length %u", data_len);
		return NULL;
	}

	struct player_node *node = player_search_bssid_internal(net->bssid);

	if (!node) {
		LOGE(MACSTR ": not found", MAC2STR(net->bssid));
		return NULL;
	}

	if (ip->addr != node->player.ip.addr) {
		LOGE(MACSTR ": packet IP %s does not match player IP %s",
				MAC2STR(net->bssid), ip4addr_ntoa(ip),
				ip4addr_ntoa(&node->player.ip));
		return NULL;
	}

	return node;
}

static void player_cmd_complete(struct player_node *node)
{
	xTimerStop(node->t, pdMS_TO_TICKS(5000));
	LOGD("command completed");
}

static bool player_event_add_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(node);

	LOGD("adding player " MACSTR, MAC2STR(player->bssid));
	player_add(player->bssid, &player->ip);

	return true;
}

static bool player_event_del_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);

	player_del(node);

	return  true;
}

static bool player_event_game_start_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);

	global_trigger = false;
	xTimerStop(node->t, pdMS_TO_TICKS(5000));
	player_cmd_send(node, PLAYER_CMD_START, true);
	stat_change(node, P_STAT_READY_CHECK);

	return true;
}

static bool player_event_color_parse(struct player_node *node,
		struct player_event *player)
{
	node->player.color = player->color;
	player_cmd_send(node, PLAYER_CMD_COLOR, false);
	return true;
}

static bool player_event_intensity_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);

	player_cmd_send(node, PLAYER_CMD_COLOR, false);
	return true;
}

static bool player_event_start_ack_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	bool parsed = true;

	switch (node->stat) {
	case P_STAT_READY_CHECK:
		stat_change(node, P_STAT_READY);
		// fallthrough
	case P_STAT_READY:
		player_cmd_complete(node);
		break;

	default:
		parsed = false;
	}

	return parsed;
}

static bool player_event_trigger_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	bool parsed = false;

	if (!global_trigger && (P_STAT_READY_CHECK == node->stat ||
				P_STAT_READY == node->stat ||
				P_STAT_TRIGGED == node->stat)) {
		global_trigger = true;
		player_cmd_send(node, PLAYER_CMD_TRIGGER_ACK, false);
		stat_change(node, P_STAT_TRIGGED);
		if (event_cb) {
			struct player_game_event player = {};
			player.event = PLAYER_GAME_EVENT_WIN;
			memcpy(player.bssid, node->player.bssid, 6);
			event_cb(&player);
		}
		parsed = true;
	}

	return parsed;
}

static bool player_event_block_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	bool parsed = false;

	if (P_STAT_READY_CHECK == node->stat || P_STAT_READY == node->stat) {
		player_cmd_send(node, PLAYER_CMD_BLOCK, true);
		stat_change(node, P_STAT_BLOCKED_CHECK);
		parsed = true;
	}

	return parsed;
}

static bool player_event_block_ack_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	bool parsed = false;

	if (P_STAT_BLOCKED_CHECK == node->stat) {
		player_cmd_complete(node);
		stat_change(node, P_STAT_BLOCKED);
		parsed = true;
	}

	return parsed;
}

static bool player_event_batt(struct player_node *node, enum bat_event event,
		enum bat_status status)
{
	struct player_game_event game_ev = {
		.event = PLAYER_GAME_EVENT_BATT,
		.batt = {
			.event = event,
			.status = status
		}
	};
	memcpy(game_ev.bssid, node->player.bssid, 6);
	if (event_cb) {
		event_cb(&game_ev);
	}

	return true;
}

static bool player_event_batt_low_charge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_LOW, BAT_CHARGING);
}

static bool player_event_batt_low_discharge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_LOW, BAT_DISCHARGING);
}

static bool player_event_batt_mid_charge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_MID, BAT_CHARGING);
}

static bool player_event_batt_mid_discharge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_MID, BAT_DISCHARGING);
}

static bool player_event_batt_high_charge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_HIGH, BAT_CHARGING);
}

static bool player_event_batt_high_discharge_parse(struct player_node *node,
		struct player_event *player)
{
	UNUSED_PARAM(player);
	return player_event_batt(node, BAT_EV_HIGH, BAT_DISCHARGING);
}

static void event_parse(struct player_node *node, struct player_event *player)
{
	if (!node && player->event != PLAYER_EVENT_ADD) {
		LOGW(MACSTR ": no node for event %s", MAC2STR(player->bssid),
				player_event_name[player->event]);
		return;
	}

	bool parsed = parse[player->event](node, player);

	// Parse events not parsed above
	if (!parsed) {
		LOGW("player %d: ignoring event %s, stat %s",
				node->player.number,
				player_event_name[player->event],
				player_stat_name[node->stat]);
	}
}

static bool event_ok(enum player_event_type event)
{
	bool ok = true;

	if (event < 0 || event >= __PLAYER_EVENT_MAX) {
		LOGW("ignoring unknown event 0x%X", event);
		ok = false;
	}

	return ok;
}

void player_parse(struct player_event *event)
{
	struct player_node *node, *aux;

	if (!event_ok(event->event)) {
		return;
	}

	// TODO: restringe broadcast events to some of them?
	if (0 == memcmp(PLAYER_BSSID_ALL, event->bssid, 6)) {
		list_for_each_entry_safe(node, aux, &player_list, _head) {
			event_parse(node, event);
		}
	} else {
		node = player_search_bssid_internal(event->bssid);
		event_parse(node, event);
	}

}

static void network_event(const ip4_addr_t *ip, uint_fast16_t port,
		void *data, uint_fast16_t data_len)
{
	UNUSED_PARAM(port);
	uint32_t buf[ROUNDUP_DIV(PLAYER_EVENT_MAX_LEN, sizeof(uint32_t))];
	struct player_event * const net = (struct player_event*)buf;

	// We need to copy the event because lwIP pbuffer payloads
	// are not 32-bit aligned
	memcpy(net, data, data_len);
	struct player_node *node = network_check(ip, net, data_len);

	// TODO: restringe network events to some of them?
	if (!event_ok(net->event)) {
		return;
	}
	event_parse(node, net);
}

static void player_del_all(void)
{
	struct player_node *node, *aux;

	xSemaphoreTake(sem, portMAX_DELAY);
	list_for_each_entry_safe(node, aux, &player_list, _head) {
		player_del(node);
	}
	xSemaphoreGive(sem);
}

uint_fast8_t player_num_get(void)
{
	return num_players;
}

int player_init(player_game_event_cb _event_cb)
{
	if (!udp) {
		udp = udp_new();
	}
	if (!sem) {
		sem = xSemaphoreCreateBinary();
		xSemaphoreGive(sem);
	}
	event_cb = _event_cb;
	return nev_init(udp, network_event);
}

void player_deinit(void)
{
	nev_deinit();
	if (udp) {
		udp_remove(udp);
		udp = NULL;
	}
	if (sem) {
		vSemaphoreDelete(sem);
		sem = NULL;
	}
	player_del_all();
}

uint_fast8_t player_get_bssids(uint8_t **bssids)
{
	int pos = 0;
	struct player_node *node;

	xSemaphoreTake(sem, portMAX_DELAY);
	if (!num_players) {
		goto out;
	}

	*bssids = (uint8_t*)malloc(6 * num_players);
	list_for_each_entry(node, &player_list, _head) {
		memcpy(*bssids + pos, node->player.bssid, 6);
		pos += 6;
	}

out:
	xSemaphoreGive(sem);

	return num_players;
}

