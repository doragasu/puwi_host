#ifndef _UTIL_H_
#define _UTIL_H_

#include <esp_log.h>

/// Stringify token, helper macro
#define _STR(x)		#x
/// Stringify token
#define STR(x)		_STR(x)

/// Remove compiler warnings when not using a function parameter
#define UNUSED_PARAM(x)		(void)x

/// Puts a task to sleep for some milliseconds (requires FreeRTOS).
#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)

#if !defined(MAX)
/// Returns the maximum of two numbers
#define MAX(a, b)	((a)>(b)?(a):(b))
#endif
#if !defined(MIN)
/// Returns the minimum of two numbers
#define MIN(a, b)	((a)<(b)?(a):(b))
#endif

#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

#define ROUNDUP_DIV(dividend, divisor) ((((dividend)<<1)/(divisor) + 1)>>1)

void *memdup(const void *mem, size_t len);
int token_index(const char **tokens, const char *search_str);
int hex_nibble_to_bin(char nibble);
// Assumes there are two digits per byte (e.g. if byte < 16, it has a leading '0')
int hex_byte_to_bin(const char *hex_byte);
int mac_to_bin(const char *mac, uint8_t bin[6]);

#endif //_UTIL_H_

