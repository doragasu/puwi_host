
#include <lwip/udp.h>

#include "control.h"
#include "util.h"
#include "net_ev.h"

#define NET_EV_PORT 777

static nev_cb cb = NULL;

static void udp_recv_cb(void *arg, struct udp_pcb *pcb, struct pbuf *p,
		const ip_addr_t *addr, u16_t port)
{
	UNUSED_PARAM(arg);
	UNUSED_PARAM(pcb);

	if (cb) {
		cb(&addr->u_addr.ip4, port, p->payload, p->len);
	}
	pbuf_free(p);
}

int nev_init(struct udp_pcb *socket, nev_cb event_callback)
{
	err_t err;

	err = udp_bind(socket, IP_ADDR_ANY, NET_EV_PORT);
	udp_recv(socket, udp_recv_cb, NULL);
	if (err) {
		LOGE("udp bind: %s", lwip_strerr(err));
		return 1;
	}

	cb = event_callback;

	return 0;
}

void nev_deinit(void)
{
	cb = NULL;
}


